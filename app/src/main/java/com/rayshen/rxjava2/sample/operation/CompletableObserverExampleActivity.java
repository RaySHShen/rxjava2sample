package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by rayshen on 2017/10/17.
 */

public class CompletableObserverExampleActivity extends BaseActivity {

    private static final String TAG = CompletableObserverExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        Completable completable = Completable.timer(3000, TimeUnit.MILLISECONDS);
        completable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getCompletableObserver());
    }

    private CompletableObserver getCompletableObserver() {
        return new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onComplete() {
                mTextView.append("onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onComplete");
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onError : " + e.getMessage());
            }
        };
    }
}
