package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rayshen.rxjava2.sample.R;

/**
 * Created by rayshen on 2017/10/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected Button mBtn;
    protected TextView mTextView;

    protected abstract void doSomeWork();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
        mBtn = (Button) findViewById(R.id.btn);
        mTextView = (TextView) findViewById(R.id.textView);
        mBtn.setOnClickListener(mListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private View.OnClickListener mListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch(view.getId()) {
                case R.id.btn:
                    doSomeWork();
                    break;
            }
        }
    };

}
