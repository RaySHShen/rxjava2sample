package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by rayshen on 2017/10/26.
 */

public class ReplayExampleActivity extends BaseActivity {

    private static final String TAG = ReplayExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        PublishSubject<Integer> source = PublishSubject.create();
        // bufferSize = 3 to retain 3 values to replay
        ConnectableObservable<Integer> connectableObservable = source.replay(3);
        connectableObservable.connect();

        connectableObservable.subscribe(getFirstObserver());
        source.onNext(1);
        source.onNext(2);
        source.onNext(3);
        source.onNext(4);
        source.onComplete();

        /*
         * it will emit 2, 3, 4 as (count = 3), retains the 3 values for replay
         */
        connectableObservable.subscribe(getSecondObserver());
    }

    private Observer<Integer> getFirstObserver() {
        return new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "First onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(@NonNull Integer integer) {
                mTextView.append("First onNext : value : " + integer);
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "First onNext value : " + integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("First onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "First onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("First onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "First onComplete");
            }
        };
    }

    private Observer<Integer> getSecondObserver() {
        return new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "Second onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(@NonNull Integer integer) {
                mTextView.append("Second onNext : value : " + integer);
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "Second onNext value : " + integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("Second onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "Second onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("Second onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "Second onComplete");
            }
        };
    }
}
