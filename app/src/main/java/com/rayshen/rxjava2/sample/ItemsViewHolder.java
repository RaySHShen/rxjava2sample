package com.rayshen.rxjava2.sample;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by rayshen on 2017/10/17.
 */

public class ItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected TextView mItemsTextView;
    protected OnItemClickListener mItemClickListener;

    public ItemsViewHolder(View view, OnItemClickListener itemClickListener) {
        super(view);
        mItemClickListener = itemClickListener;
        mItemsTextView = view.findViewById(R.id.textView);
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
