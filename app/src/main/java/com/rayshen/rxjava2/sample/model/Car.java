package com.rayshen.rxjava2.sample.model;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

/**
 * Created by rayshen on 2017/10/17.
 */

public class Car {
    private String mBrand;

    public void setBrand(String brand) {
        this.mBrand = brand;
    }

    public Observable<String> brandDeferObservable() {
        return Observable.defer(new Callable<ObservableSource<? extends String>>() {
            @Override
            public ObservableSource<? extends String> call() throws Exception {
                return Observable.just(mBrand);
            }
        });
    }
}
