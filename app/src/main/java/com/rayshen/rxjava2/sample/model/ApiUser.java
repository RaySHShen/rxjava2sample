package com.rayshen.rxjava2.sample.model;

/**
 * Created by rayshen on 2017/10/17.
 */

public class ApiUser {
    public long mId;
    public String mFirstName;
    public String mLastName;

    @Override
    public String toString() {
        return "ApiUser{" +
                "id=" + mId +
                ", firstname='" + mFirstName + '\'' +
                ", lastname='" + mLastName + '\'' +
                '}';
    }
}
