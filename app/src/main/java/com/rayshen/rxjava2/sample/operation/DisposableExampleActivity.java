package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by rayshen on 2017/10/16.
 */

public class DisposableExampleActivity extends BaseActivity {

    private static final String TAG = DisposableExampleActivity.class.getSimpleName();

    private final CompositeDisposable mDisposables = new CompositeDisposable();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mDisposables != null) {
            mDisposables.clear();
        }
    }

    @Override
    protected void doSomeWork() {
        mDisposables.add(getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    public void onNext(@NonNull String s) {
                        mTextView.append(s);
                        mTextView.append(AppConstant.LINE_SEPARATOR);
                        Log.d(TAG, "onNext : " + s);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mTextView.append("onError : " + e.getMessage());
                        mTextView.append(AppConstant.LINE_SEPARATOR);
                        Log.d(TAG, "onError : " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mTextView.append("onComplete");
                        mTextView.append(AppConstant.LINE_SEPARATOR);
                        Log.d(TAG, "onComplete");
                    }
                }));
    }

    private Observable<String> getObservable() {
        return Observable.defer(new Callable<ObservableSource<? extends String>>() {
            @Override
            public ObservableSource<? extends String> call() throws Exception {
                SystemClock.sleep(2000);
                return Observable.just("one", "two", "three", "four", "five");
            }
        });
    }
}
