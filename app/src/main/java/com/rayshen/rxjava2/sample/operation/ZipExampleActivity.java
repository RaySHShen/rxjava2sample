package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;
import com.rayshen.rxjava2.sample.Utils;
import com.rayshen.rxjava2.sample.model.User;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by rayshen on 2017/10/26.
 */

public class ZipExampleActivity extends BaseActivity {

    private static final String TAG = ZipExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        Observable.zip(getCricketFansObservable(), getFootballFansObservable(),
                new BiFunction<List<User>, List<User>, List<User>>() {
                    @Override
                    public List<User> apply(List<User> cricketFans, List<User> footballFans) throws Exception {
                        return Utils.filterUserWhoLovesBoth(cricketFans, footballFans);
                    }
                })
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());
    }

    private Observable<List<User>> getCricketFansObservable() {
        return Observable.create(new ObservableOnSubscribe<List<User>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<User>> e) throws Exception {
                if (!e.isDisposed()) {
                    e.onNext(Utils.getUserListWhoLovesCricket());
                    e.onComplete();
                }
            }
        });
    }

    private Observable<List<User>> getFootballFansObservable() {
        return Observable.create(new ObservableOnSubscribe<List<User>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<User>> e) throws Exception {
                if (!e.isDisposed()) {
                    e.onNext(Utils.getUserListWhoLovesFootball());
                    e.onComplete();
                }
            }
        });
    }

    private Observer<List<User>> getObserver() {
        return new Observer<List<User>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(@NonNull List<User> users) {
                mTextView.append("onNext");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                for (User user : users) {
                    mTextView.append("firstname : " + user.mFirstName);
                    mTextView.append(AppConstant.LINE_SEPARATOR);
                }
                Log.d(TAG, "onNext : " + users.size());
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onComplete");
            }
        };
    }
}
