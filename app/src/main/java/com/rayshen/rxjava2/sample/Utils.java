package com.rayshen.rxjava2.sample;

import android.util.Log;

import com.androidnetworking.error.ANError;

import com.rayshen.rxjava2.sample.model.ApiUser;
import com.rayshen.rxjava2.sample.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rayshen on 2017/10/25.
 */

public class Utils {
    public static List<User> getUserList() {

        List<User> userList = new ArrayList<>();

        User userOne = new User();
        userOne.mFirstName = "Amit";
        userOne.mLastName = "Shekhar";
        userList.add(userOne);

        User userTwo = new User();
        userTwo.mFirstName = "Manish";
        userTwo.mLastName = "Kumar";
        userList.add(userTwo);

        User userThree = new User();
        userThree.mFirstName = "Sumit";
        userThree.mLastName = "Kumar";
        userList.add(userThree);

        return userList;
    }

    public static List<ApiUser> getApiUserList() {

        List<ApiUser> apiUserList = new ArrayList<>();

        ApiUser apiUserOne = new ApiUser();
        apiUserOne.mFirstName = "Amit";
        apiUserOne.mLastName = "Shekhar";
        apiUserList.add(apiUserOne);

        ApiUser apiUserTwo = new ApiUser();
        apiUserTwo.mFirstName = "Manish";
        apiUserTwo.mLastName = "Kumar";
        apiUserList.add(apiUserTwo);

        ApiUser apiUserThree = new ApiUser();
        apiUserThree.mFirstName = "Sumit";
        apiUserThree.mLastName = "Kumar";
        apiUserList.add(apiUserThree);

        return apiUserList;
    }

    public static List<User> convertApiUserListToUserList(List<ApiUser> apiUserList) {

        List<User> userList = new ArrayList<>();

        for (ApiUser apiUser : apiUserList) {
            User user = new User();
            user.mFirstName = apiUser.mFirstName;
            user.mLastName = apiUser.mLastName;
            userList.add(user);
        }

        return userList;
    }

    public static List<User> getUserListWhoLovesCricket() {

        List<User> userList = new ArrayList<>();

        User userOne = new User();
        userOne.mId = 1;
        userOne.mFirstName = "Amit";
        userOne.mLastName = "Shekhar";
        userList.add(userOne);

        User userTwo = new User();
        userTwo.mId = 2;
        userTwo.mFirstName = "Manish";
        userTwo.mLastName = "Kumar";
        userList.add(userTwo);

        return userList;
    }


    public static List<User> getUserListWhoLovesFootball() {

        List<User> userList = new ArrayList<>();

        User userOne = new User();
        userOne.mId = 1;
        userOne.mFirstName = "Amit";
        userOne.mLastName = "Shekhar";
        userList.add(userOne);

        User userTwo = new User();
        userTwo.mId = 3;
        userTwo.mFirstName = "Sumit";
        userTwo.mLastName = "Kumar";
        userList.add(userTwo);

        return userList;
    }


    public static List<User> filterUserWhoLovesBoth(List<User> cricketFans, List<User> footballFans) {
        List<User> userWhoLovesBoth = new ArrayList<User>();
        for (User cricketFan : cricketFans) {
            for (User footballFan : footballFans) {
                if (cricketFan.mId == footballFan.mId) {
                    userWhoLovesBoth.add(cricketFan);
                }
            }
        }
        return userWhoLovesBoth;
    }

    public static void logError(String TAG, Throwable e) {
        if (e instanceof ANError) {
            ANError anError = (ANError) e;
            if (anError.getErrorCode() != 0) {
                // received ANError from server
                // error.getErrorCode() - the ANError code from server
                // error.getErrorBody() - the ANError body from server
                // error.getErrorDetail() - just a ANError detail
                Log.d(TAG, "onError errorCode : " + anError.getErrorCode());
                Log.d(TAG, "onError errorBody : " + anError.getErrorBody());
                Log.d(TAG, "onError errorDetail : " + anError.getErrorDetail());
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Log.d(TAG, "onError errorDetail : " + anError.getErrorDetail());
            }
        } else {
            Log.d(TAG, "onError errorMessage : " + e.getMessage());
        }
    }
}
