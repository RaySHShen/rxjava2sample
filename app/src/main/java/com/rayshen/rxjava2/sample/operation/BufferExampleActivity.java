package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by rayshen on 2017/10/26.
 */

public class BufferExampleActivity extends BaseActivity {

    private static final String TAG = BufferExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        // 3 means, it takes max of three from its start index and create list
        // 1 means, it jumps one step every time
        // so the it gives the following list
        // 1 - one, two, three
        // 2 - two, three, four
        // 3 - three, four, five
        // 4 - four, five
        // 5 - five
        Observable<List<String>> buffered = getObservable().buffer(3, 1);
        buffered.subscribe(getObserver());
    }

    private Observable<String> getObservable() {
        return Observable.just("one", "two", "three", "four", "five");
    }

    private Observer<List<String>> getObserver() {
        return new Observer<List<String>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(@NonNull List<String> strings) {
                mTextView.append("onNext size : " +  strings.size());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                for (String value : strings) {
                    mTextView.append("value : " + value);
                    mTextView.append(AppConstant.LINE_SEPARATOR);
                    Log.d(TAG, " : value :" + value);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onComplete");
            }
        };
    }
}
