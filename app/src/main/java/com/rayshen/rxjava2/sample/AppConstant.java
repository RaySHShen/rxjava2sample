package com.rayshen.rxjava2.sample;

import com.rayshen.rxjava2.sample.operation.BufferExampleActivity;
import com.rayshen.rxjava2.sample.operation.CompletableObserverExampleActivity;
import com.rayshen.rxjava2.sample.operation.ConcatExampleActivity;
import com.rayshen.rxjava2.sample.operation.DeferExampleActivity;
import com.rayshen.rxjava2.sample.operation.DisposableExampleActivity;
import com.rayshen.rxjava2.sample.operation.FilterExampleActivity;
import com.rayshen.rxjava2.sample.operation.FlowableExampleActivity;
import com.rayshen.rxjava2.sample.operation.IntervalExampleActivity;
import com.rayshen.rxjava2.sample.operation.MapExampleActivity;
import com.rayshen.rxjava2.sample.operation.MergeExampleActivity;
import com.rayshen.rxjava2.sample.operation.ReduceExampleActivity;
import com.rayshen.rxjava2.sample.operation.ReplayExampleActivity;
import com.rayshen.rxjava2.sample.operation.SingleObserverExampleActivity;
import com.rayshen.rxjava2.sample.operation.SkipExampleActivity;
import com.rayshen.rxjava2.sample.operation.TakeExampleActivity;
import com.rayshen.rxjava2.sample.operation.ZipExampleActivity;

/**
 * Created by rayshen on 2017/10/16.
 */

public class AppConstant {
    public static final String LINE_SEPARATOR = "\n";
    public static final Class[] ACTIVITY_ARRAY = {
            DisposableExampleActivity.class,
            FlowableExampleActivity.class,
            SingleObserverExampleActivity.class,
            CompletableObserverExampleActivity.class,
            MapExampleActivity.class,
            ZipExampleActivity.class,
            BufferExampleActivity.class,
            TakeExampleActivity.class,
            ReduceExampleActivity.class,
            FilterExampleActivity.class,
            SkipExampleActivity.class,
            ReplayExampleActivity.class,
            ConcatExampleActivity.class,
            MergeExampleActivity.class,
            DeferExampleActivity.class,
            IntervalExampleActivity.class
    };
}
