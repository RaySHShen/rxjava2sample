package com.rayshen.rxjava2.sample;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rayshen on 2017/10/17.
 */

public class ItemsAdapter extends RecyclerView.Adapter<ItemsViewHolder> {

    private Context mContext;
    private String[] mTitles;

    public ItemsAdapter(Context context) {
        mContext = context;
        mTitles = context.getResources().getStringArray(R.array.activity_items);
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.items_adapter, parent, false);
        ItemsViewHolder itemsViewHolder = new ItemsViewHolder(view, mItemClickListener);
        return itemsViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemsViewHolder holder, int position) {
        holder.mItemsTextView.setTag(position);
        holder.mItemsTextView.setText(mTitles[position]);
    }

    @Override
    public int getItemCount() {
        return mTitles.length;
    }

    private ItemsViewHolder.OnItemClickListener mItemClickListener = new ItemsViewHolder.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position) {
            Log.d("Ray", "Class : " + AppConstant.ACTIVITY_ARRAY[position].getName());
            mContext.startActivity(new Intent(mContext, AppConstant.ACTIVITY_ARRAY[position]));
        }
    };
}
