package com.rayshen.rxjava2.sample.model;

/**
 * Created by rayshen on 2017/10/17.
 */

public class User {
    public long mId;
    public String mFirstName;
    public String mLastName;
    public boolean mIsFollowing;

    public User() {
    }

    public User(ApiUser apiUser) {
        this.mId = apiUser.mId;
        this.mFirstName = apiUser.mFirstName;
        this.mLastName = apiUser.mLastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + mId +
                ", firstname='" + mFirstName + '\'' +
                ", lastname='" + mLastName + '\'' +
                ", isFollowing=" + mIsFollowing +
                '}';
    }
}
