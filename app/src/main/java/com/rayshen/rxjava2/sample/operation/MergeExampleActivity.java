package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by rayshen on 2017/10/26.
 */

public class MergeExampleActivity extends BaseActivity {

    private static final String TAG = MergeExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        final String[] aStrings = {"A1", "A2", "A3", "A4"};
        final String[] bStrings = {"B1", "B2", "B3"};

        final Observable<String> aObservable = Observable.fromArray(aStrings);
        final Observable<String> bObservable = Observable.fromArray(bStrings);

        Observable.merge(aObservable, bObservable)
                .subscribe(getObserver());
    }

    private Observer<String> getObserver() {
        return new Observer<String>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(String value) {
                mTextView.append("onNext : value : " + value);
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onNext : value : " + value);
            }

            @Override
            public void onError(Throwable e) {
                mTextView.append("onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onComplete");
            }
        };
    }
}
