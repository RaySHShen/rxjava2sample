package com.rayshen.rxjava2.sample.operation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.rayshen.rxjava2.sample.AppConstant;
import com.rayshen.rxjava2.sample.model.Car;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by rayshen on 2017/10/26.
 */

public class DeferExampleActivity extends BaseActivity {

    private static final String TAG = DeferExampleActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void doSomeWork() {
        Car car = new Car();
        Observable<String> brandDeferObservable = car.brandDeferObservable();
        car.setBrand("BMW");
        brandDeferObservable.subscribe(getObserver());
    }

    private Observer<String> getObserver() {
        return new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Log.d(TAG, "onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(@NonNull String string) {
                mTextView.append("onNext : value : " + string);
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onNext : value : " + string);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mTextView.append("onError : " + e.getMessage());
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
                mTextView.append("onComplete");
                mTextView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, "onComplete");
            }
        };
    }
}
